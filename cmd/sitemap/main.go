package main

func Start() error {
	sitesStore, err := sitesMySQL.New(
		"v3dev",
		"v3dev",
		"dbb1_analytics",
	)

	if err != nil {
		return err
	}

	sitesHandler, err := sitesHandler.New(
		sitesStore,
	)

	if err != nil {
		return err
	}

	sitesInterface, err := sitesCLI.New(
		sitesHandler,
	)

	if err != nil {
		return err
	}

	return sitesInterface.ReadSitemap()
}

func main() {
	log.Println("App starting, hello!")
	if err := Start(); err != nil {
		panic(err)
	}

	log.Println("App quitting, bye!")
}